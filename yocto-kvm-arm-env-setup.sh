#!/bin/bash


# Original script by G.Papaux
# Modifications by Vincenzo Do


# Catch errors
trap check_error ERR

# Configuration variables
SETUP_DIR=$PWD
BUILD_DIR="build"
BBLAYER_CONF_FILE=conf/bblayers.conf

POKY_GIT="git://git.yoctoproject.org/poky.git"
POKY_COMMIT_ID="9c4ff467f66428488b1cd9798066a8cb5d6b4c3b"
POKY_LOCAL_BRANCH="dizzy"
POKY_DESTDIR="poky-dizzy"

LAYERS_DIR=$SETUP_DIR/$POKY_DESTDIR

# Deps list
# Format: GIT_URL;LOCAL_DEST_DIR;COMMIT_ID;LOCAL_BRANCH
DEPS=" \
  git://git.openembedded.org/meta-openembedded;meta-openembedded;5b6f39ce325d490fc382d5d59c5b8b9d5fa38b38;dizzy.06.2015 \
  git://git.yoctoproject.org/meta-virtualization;meta-virtualization;1c1be83047035da25a14c28ac1f9453f7fdc67bc;dizzy.06.2015 \
  git://git.yoctoproject.org/meta-ti;meta-ti;cf929ea6564564458eeabfd4fb4d42a86b20cd85;dizzy.06.2015"
  
  
# Patches
# Format: PATCH_FILE_PATH;APPLICATION_PATH
PATCHES=" \
  "

########################################################################
# Functions definition
########################################################################


function check_error () {
	echo "An error occured, exiting configuration script..."
	exit
}

function check_already_initialized () {
	if [ -d "$POKY_DESTDIR" ]; then
		echo "Your yocto workspace is already initialized..."
		echo "Run the following command to jump to your build dir"
		echo
		echo "cd $POKY_DESTDIR"
		echo "source oe-init-build-env"
		exit
	fi
}

function check_working_directory () {
	if [ ! -f $PWD/$(basename $0) ]; then
		echo "This script has to be executed from the current directory, i.e"
		echo ./$(basename $0)
		exit
	fi
}


# First step is to fetch poky...
function fetch_poky () {
	git clone $POKY_GIT $POKY_DESTDIR
	cd $POKY_DESTDIR
	git checkout $POKY_COMMIT_ID -b $POKY_LOCAL_BRANCH
}


# Fetch depedencies defined in $DEPS variable
function fetch_dependencies () {

	for dep in $DEPS
	do
		info=(${dep//;/ })
		repo=${info[0]}
		dst=${info[1]}
		commit=${info[2]}
		localbranch=${info[3]}

		# if already exists...
		if [ -d "$dst" ]; then
			echo
			echo "ERROR: Repository $dst already exists... it seems you already initialized this working directory!"
			echo "Run the following command to jump to your build dir"
			echo
			echo "source oe-init-build-env"
			echo
			exit
		fi

		echo "Cloning at $commit from $repo into local branch $localbranch"
		git clone $repo $dst
		cd $dst

		# want to create a new local branch ?
		if [ -n "$localbranch" ]; then
			git checkout $commit -b $localbranch
		else
			git checkout $commit
		fi

		cd $LAYERS_DIR
	done

}


# Apply patches
function apply_patches () {
	for patch in $PATCHES
	do
		info=(${patch//;/ })
		file=${info[0]}
		target=${info[1]}
		echo "Applying patch basename($file) for $target..."
		cd $target
		git apply $file
		cd $LAYERS_DIR
	done

}


function configure_environment () {

	cp $SETUP_DIR/include/kvm-image-extended.bb $SETUP_DIR/$POKY_DESTDIR/meta-virtualization/recipes-extended/images/
	cp $SETUP_DIR/include/omap5-evm-kvm.conf $SETUP_DIR/$POKY_DESTDIR/meta-ti/conf/machine/
    cp $SETUP_DIR/include/omap-a15-kvm.inc $SETUP_DIR/$POKY_DESTDIR/meta-ti/conf/machine/include/
    cp -Rf $SETUP_DIR/include/linux-yocto_3.19_2/ $SETUP_DIR/$POKY_DESTDIR/meta/recipes-kernel/linux/
    cp $SETUP_DIR/include/linux-yocto_3.19_2.bb  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-kernel/linux/
    cp $SETUP_DIR/include/defconfig  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-kernel/linux/
    cp $SETUP_DIR/include/0001-omap5-kvm-patch-dts-for-vgic-support-in-kvm.patch  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-kernel/linux/
    cp $SETUP_DIR/include/python_2.7.9.bb  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-devtools/python/
    cp $SETUP_DIR/include/linux-yocto_3.19.bb  $SETUP_DIR/$POKY_DESTDIR/meta-virtualization/recipes-kernel/linux
    cp $SETUP_DIR/include/u-boot-omap5-kvm_2013.07.bb  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-bsp/u-boot/
    cp $SETUP_DIR/include/u-boot-omap-kvm-boot.src.sd.3-19  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-bsp/u-boot/
	cp -Rf $SETUP_DIR/include/u-boot-omap5-kvm/  $SETUP_DIR/$POKY_DESTDIR/meta/recipes-bsp/u-boot/
	
	source oe-init-build-env > /dev/null

	# customize configuration
	generate_bblayer_conf
	echo
	echo "Done."
	echo "Run the following commands to jump to your build dir"
	echo "   cd $POKY_DESTDIR"
	echo "   source oe-init-build-env"
	echo
	echo "And you can buidl the default kvm target with these commands"
	echo "   MACHINE=omap5-evm-kvm bitbake kvm-image-extended"
	echo "   MACHINE=odroidxu-kvm bitbake kvm-image-extended"
}


function generate_bblayer_conf () {

	# backup old file
	mv $BBLAYER_CONF_FILE ${BBLAYER_CONF_FILE}.bak

	# reset file
	> $BBLAYER_CONF_FILE

	# ugly but....
	# default values
	echo '
# LAYER_CONF_VERSION is increased each time build/conf/bblayers.conf
# changes incompatibly
LCONF_VERSION = "6"

BBPATH = "${TOPDIR}"
BBFILES ?= ""

BBLAYERS_NON_REMOVABLE ?= " \
  '"${LAYERS_DIR}"'/meta \
  '"${LAYERS_DIR}"'/meta-yocto \
  "
' > $BBLAYER_CONF_FILE


	# add layers
	echo '

BBLAYERS ?= " \
  '"${LAYERS_DIR}"'/meta \
  '"${LAYERS_DIR}"'/meta-yocto \
  '"${LAYERS_DIR}"'/meta-yocto-bsp \
  '"${LAYERS_DIR}"'/meta-openembedded/meta-oe \
  '"${LAYERS_DIR}"'/meta-openembedded/meta-networking \
  '"${LAYERS_DIR}"'/meta-openembedded/meta-python \
  '"${LAYERS_DIR}"'/meta-virtualization \
  '"${LAYERS_DIR}"'/meta-ti \
  "
' >> $BBLAYER_CONF_FILE
}

########################################################################
# Entry code
########################################################################
echo "########################################################################"
echo "# Initialization script for setting up poky for KVM/ARM virtualization"
echo "# Created by Geoffrey Papaux <geoffrey.papaux gmail com>"
echo "# Modified by Vincenzo Do <vincenzo.do master.hes-so.ch>"
echo "########################################################################"
echo

# Check if already initialized
check_already_initialized

# Check if we are in the working directory...
check_working_directory

# First step is to fetch poky
echo
echo "Getting poky..."
echo "#########################"
fetch_poky

# Download required layers
echo
echo "Getting required layers..."
echo "#########################"
fetch_dependencies

# Apply patches needed for our target
echo
echo "Applying patches..."
echo "#########################"
apply_patches

# Configure environment as required for yocto build
echo
echo "Configuring build environment..."
echo "#########################"
configure_environment
