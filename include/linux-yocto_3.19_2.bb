
KBRANCH ?= "standard/base"

require recipes-kernel/linux/linux-yocto.inc

# board specific branches
KBRANCH_qemuarm  ?= "standard/arm-versatile-926ejs"
KBRANCH_omap-a15  ?= "standard/arm-versatile-926ejs"
KBRANCH_omap5  ?= "standard/arm-versatile-926ejs"

SRCREV_machine_qemuarm ?= "473e2f3788730c51e82714a9785325b6308f6762"
SRCREV_machine_omap-a15 ?= "473e2f3788730c51e82714a9785325b6308f6762"
SRCREV_machine_omap5 ?= "473e2f3788730c51e82714a9785325b6308f6762"
SRCREV_meta ?= "9e70b482d3773abf92c9c5850e134cbca1d5651f"

KERNEL_DEVICETREE_omap5-evm = "omap5-uevm.dtb"

# Pull in the devicetree files into the rootfs
RDEPENDS_kernel-base += "kernel-devicetree"

# Add a run-time dependency for the PM firmware to be installed
# on the target file system.
RDEPENDS_kernel-base_append_ti33x = " am33x-cm3"
RDEPENDS_kernel-base_append_ti43x = " am33x-cm3"

# Add a run-time dependency for the VPE VPDMA firmware to be installed
# on the target file system.
RDEPENDS_kernel-base_append_dra7xx = " vpdma-fw"
KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT}"

SRC_URI = "git://git.yoctoproject.org/linux-yocto-3.19.git;bareclone=1;branch=${KBRANCH},${KMETA};name=machine,meta"

KERNEL_CONFIG_DIR = "/home/vince/Documents/Master/S2/PA/"

SRC_URI += "file:///home/vince/Documents/Master/S2/PA/env/poky/meta/recipes-kernel/linux/defconfig \
           "
SRC_URI_append_omap5-evm-kvm = "file:///home/vince/Documents/Master/S2/PA/env/poky/meta/recipes-kernel/linux/0001-omap5-kvm-patch-dts-for-vgic-support-in-kvm.patch \
                                "

LINUX_VERSION ?= "3.19.2"

PV = "${LINUX_VERSION}+git${SRCPV}"

KMETA = "meta"
KCONF_BSP_AUDIT_LEVEL = "2"

COMPATIBLE_MACHINE = "qemuarm|qemuarm64|qemux86|qemuppc|qemumips|qemumips64|qemux86-64|omap5|omap-a15"

# Functionality flags
KERNEL_EXTRA_FEATURES ?= "features/netfilter/netfilter.scc"
KERNEL_FEATURES_append = " ${KERNEL_EXTRA_FEATURES}"
KERNEL_FEATURES_append_qemux86=" cfg/sound.scc cfg/paravirt_kvm.scc"
KERNEL_FEATURES_append_qemux86-64=" cfg/sound.scc cfg/paravirt_kvm.scc"
KERNEL_FEATURES_append = " ${@bb.utils.contains("TUNE_FEATURES", "mx32", " cfg/x32.scc", "" ,d)}"
