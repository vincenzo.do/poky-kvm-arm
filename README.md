# Build system for Linux and KVM on ARM platforms

This repository helps you to prepare and build a basic Linux Kernel and roofts with KVM virtualization stack.

The installation script **yocto-kvm-arm-env-setup.sh** will setup a ready-to-use poky-daisy Yocto distribution with meta layers for providing KVM on ARM:
 - *meta-virtualization*: support for building virtualization stack (KVM, libvirt, etc.)
 - *meta-ti*: basic support for TI boards (OMAP 5432)


The supported boards until now are:
- TI OMAP 5432


The generated images contain following elements:


|              | TI OMAP 5432 uEVM | 
| ------------ | ----------------- | 
| Machine name | omap5-evm-kvm     |
| Linux Kernel | Patched 3.19.2    | 
| U-Boot       | Patched 2013.07   | 


The rootfs generated will contain following virtualization software stack:

- QEMU 2.3.0
- libvirt 1.2.14


## Installation


Use the following commands to get started:

    # clone this repository
    git clone https://forge.tic.eia-fr.ch/git/vincenzo.do/poky-kvm-arm.git

    # change directory
    cd poky-kvm-arm

    # execute initialization script
    ./yocto-kvm-arm-env-setup.sh



## Create image

After installing the required elements, you can generate your first kernel image with rootfs and u-boot:

    # go to poky-dora directory
    cd poky-daisy

    # configure build env
    source oe-init-build-env

    # build a minimal kvm image
    MACHINE=omap5-evm-kvm bitbake kvm-image-extended

Note that we use the **omap5-evm-kvm** machine, simply give another machine name to generate the image for other platforms.


## Deploy image

The generated images are available in this directory: BUILD_DIR/tmp/deploy/images/*machine-name*/

You can deploy these images to the target. The U-Boot image and MLO/SPL files have to be copied on the SD Card while Kernel and Rootfs can either be copied to the SD Card or accessed through NFS/TFTP.

The process for creating the bootable SD card depends on the board.

### TI OMAP 5432 uEVM

A script initializing the SD-Card is provided as part of the meta-kvm-arm layer. It will:

* First clear the SD-Card
* Create two partitions: boot and rootfs
* Copy the following files from u-boot to the boot partition
 * MLO
 * u-boot.img
 * uImage
 * uImage-omap5-uevm.dtb -> omap5-uevm.dtb
 * u-boot-omap-kvm-boot.src.sd.3-12 -> boot.scr
* Extract the root file system (kvm-image-extended-omap5-evm-kvm.tar.gz) to the rootfs partition


#### Burn to SD-Card

The script is automatically copied to the odroid image directory when you build with bitbake/hob, so you can simply call it with the device corresponding to the SD-Card:

    cd tmp/deploy/images/omap5-evm-kvm
    sudo ./install_on_sd_card.sh --device /dev/sdd


If the script succeeds, you should see this message at the end:

    Done. It is now safe to eject and remove your SD-Card.

You should now have SD-Card with the software stack compiled previously ready to use.

## Clean

If you want to clean your local build / configuration, simply delete the poky-daisy folder and re-run the environment initialization script
    rm -rf poky-daisy
    ./yocto-kvm-arm-env-setup.sh

